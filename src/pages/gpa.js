import React from 'react';
import Container from 'react-bootstrap/Container'
// import TableGpa from '../component/TableGpa';
import Form from 'react-bootstrap/Form'
// import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
// import Nodata from '../component/Nodata';
import {gpadatas} from './GpaData.js';
import GpaFunc from '../component/gpafunc';
import './gpa.css'
class Gpa extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            sems: gpadatas, 
            semValue: 'All',
            yearValue: 'All',


        }
    }
    handleChangeSem = (event) => {
        console.log(this.state.semValue);
        this.setState({ semValue: event.target.value });
        console.log(this.state.semValue);
    }
    handleChangeYear = (event) => {
        this.setState({ yearValue: event.target.value });
        console.log(this.state.yearValue);
    }
    render() {
        return (
            <div>
                <br />
                <Container>
                    <div className="row justify-content-center">
                        <div className="col-5 ">
                            <h3><i class="fas fa-university"></i>&nbsp;Education</h3>
                            <hr />
                            <ul className="ul1">
                                <li><b>StudentID</b> : 602115020</li>
                                <li><b>Name Surname</b> : Poomrapee Kanthapong</li>
                                <li><b>Education level</b> : Bachelor degree</li>
                                <li><b>Faculty</b> : College of Arts, Media and Technology</li>
                                <li><b>Major</b> : Software Engineering</li>
                            </ul>
                        </div>
                    </div>
                </Container>
                <br />
                <Container>
                    <Form>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridState">
                                <Form.Label><b>Semester</b></Form.Label>
                                <Form.Control as="select" value={this.state.semValue} onChange={this.handleChangeSem}>
                                    <option value="All">All</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="Summer">Summer</option>
                                </Form.Control>

                            </Form.Group>
                            &nbsp;
                            <Form.Group as={Col} controlId="formGridState">
                                <Form.Label><b>Year</b></Form.Label>
                                <Form.Control as="select" value={this.state.yearValue} onChange={this.handleChangeYear}>
                                    <option value="All">All</option>
                                    <option value="2560">2560</option>
                                    <option value="2561">2561</option>
                                    <option value="2562">2562</option>
                                    <option value="2563">2563</option>
                                </Form.Control>
                            </Form.Group>
                        </Form.Row>
                    </Form>
                </Container>
                <br />
                <Container>
                    
                    <GpaFunc 
                        semValue={this.state.semValue}
                        yearValue={this.state.yearValue}
                        sems={this.state.sems}
                    />
                    {/* <a href="/processLogout" >Log out</a> */}
                </Container>
            </div>


        );
    }
}
export default Gpa;