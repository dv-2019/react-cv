import React from 'react';
import Container from 'react-bootstrap/Container'
// import './aboutme.css'
const Preferences = () => {
    return (
        <Container  className="rowAbout">
            <br />
            <h3><i class="fas fa-asterisk"></i>&nbsp;<b>Preferences</b></h3>
            <hr />
            
            <ul>
                <li><i class="fa fa-book"></i>&nbsp;
                    <b>Novel</b>
                </li>
                
                <li><i class="fa fa-gamepad"></i>&nbsp;<b>Video Games</b> </li>
                <div id="skill"></div>
                <li><i class="fa fa-video-camera"></i>&nbsp;<b>Adult Video</b> </li>
            </ul>
        </Container>
    )
} 
export default Preferences;