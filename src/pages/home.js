import React from 'react';
import AboutMe from './AboutMePage';
import Preferences from './preference';
import Skill from './SkillBar.js'
import History from './historypage';
class Home extends React.Component{
    render (){
        return (
            
            <div >
                <div id="aboutme"></div>
                <div ></div>
                <br />
                <AboutMe  />
                <Preferences />
                <Skill  />
                <History />
                <br />
            </div>
            

        );
    }
}
export default Home;