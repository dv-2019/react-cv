import React from 'react';
import Container from 'react-bootstrap/Container'
import Carousel from 'react-bootstrap/Carousel'
import './aboutme.css'
const History = () => {
    return (
        <Container  className="rowAbout">
            <h3><i class="fas fa-images"></i>&nbsp;<b>History</b></h3>
            <hr></hr>
            <Carousel>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="./1.jpg"
                        alt="First slide"
                    />
                    <Carousel.Caption>
                        <h3>Picture when i was young</h3>
                        <p>I went home during the holidays and found this picture.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="./2.jpg"
                        alt="Third slide"
                    />

                    <Carousel.Caption>
                        <h3>Cooking</h3>
                        <p>Cooking on a canal dig day.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="./3.jpg"
                        alt="Third slide"
                    />

                    <Carousel.Caption>
                        <h3>Japan</h3>
                        <p>Went to Japan with the group for a study trip.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </Container>
    )
}
export default History;