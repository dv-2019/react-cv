import React from 'react';
import Container from 'react-bootstrap/Container'

import './skill.css'
import './aboutme.css'
class Skill extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
            skills :[
                { type: "HTML", level: 85 },
                { type: "CSS", level: 70 },
                { type: "Java", level: 55 },
                { type: "JavaScript", level: 60 },
                { type: "jQuery", level:  40},
                { type: "BootStrap", level: 50 },
                { type: "Angular.js", level: 45 },
                { type: "React.js", level: 25 },
                
            ]
        };
    }
    
    render() {
        const { skills } = this.state;
        return (
            
            <Container  className="rowAbout">
                <br />
                <h3><i className="fas fa-star"></i>&nbsp;<b>Speciality</b></h3>
                <hr></hr>
                <Container>
                    <h4><i className="fas fa-language"></i>&nbsp;<b>Languages</b></h4>
                    <ul>
                        <li>Thai</li>
                        <li>English </li>
                    </ul>
                </Container>
                <Container >
                    <h4><i className="fas fa-code"></i>&nbsp;<b>Coding Skill</b></h4>
                    <hr></hr>
                    <ul className="skills">
                    
                        {skills.map((skill, index) =>
                            <li
                                key={skill.type}
                                style={{ width: `${skill.level}%`, backgroundColor: `hsl(${200}, ${40}%, ${100 / (index + 3.5)}%)` }}
                            >
                                <p>{skill.type}<span>{skill.level}</span></p>
                            </li>
                            
                        )}
                    </ul>
                    
                    
                </Container>
                <div id="history"></div>
                <hr />
                <br />
            </Container>
        );
    }
}

export default Skill;