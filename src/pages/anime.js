
import React, { useEffect, useState } from 'react';
// import { Table, Divider, Tag } from 'antd';
// import { Row, Col } from 'antd';
import Container from 'react-bootstrap/Container'
// import AnimeCard from './animeCard'
// import Nodata from '../component/Nodata';
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import { Input } from 'antd';
import AnimeList from './animeList';
import { Select } from 'antd';

const { Option } = Select;
const Anime = () => {
    const [animes, setAnimes] = useState([]);
    const [query, setQuery] = useState('');
    const [isLoading, setLoading] = useState(true);
    const [type, setType] = useState('All');
    const [types, setTypes] = useState([])
    useEffect(() => {
        fetchAnime();
    }, [])
    const fetchAnime = () => {
        fetch('https://api.jikan.moe/v3/search/anime?q=keroro')
            .then(response => response.json())
            .then(data => {
                setAnimes(data["results"])
                let filterItems = types;
                data.results.map((anime) => {
                    if (!filterItems.find(x => x === anime.type)) {
                        filterItems.push(anime.type);
                    }
                });
                setTypes(filterItems);
                setTimeout(() => {
                    setLoading(false)
                }, 500)

            })
            .catch(error => console.log(error));
    }
    const fetchAnimeQuery = () => {
        fetch('https://api.jikan.moe/v3/search/anime?q=' + query)
            .then(response => response.json())
            .then(data => {
                setAnimes(data["results"])
                let filterItems = types;
                data.results.map((anime) => {
                    if (!filterItems.find(x => x === anime.type)) {
                        filterItems.push(anime.type);
                    }
                });
                setTypes(filterItems);
                setTimeout(() => {
                    setLoading(false)
                }, 500)
            })
            .catch(error => console.log(error));
    }
    const handleChangeQuery = (event) => {
        const query = event.target.value;
        setQuery(query);
        fetchAnimeQuery();

    }

    const onChange = (value) => {
        setType(value);
    }

    return (
        <div>
            <br />
            <Container>
                <Row>
                    <Col>
                        <Input placeholder="input text to search" value={query} onChange={handleChangeQuery} />
                    </Col>
                    <Col>

                        <Select
                            showSearch
                            style={{ width: 200 }}
                            placeholder="Select a type"
                            optionFilterProp="children"
                            onChange={onChange}
                            filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            <Option value='All'>All</Option>
                            {types.map((data, index) => {
                                return (
                                    <Option value={index}>{data}</Option>
                                )
                            })}
                        </Select>
                    </Col>
                </Row>
            </Container>
            <br />

            <Container>
                <AnimeList data={type === 'All' ? animes : animes.filter(m => m.type === types[type])} load={isLoading} type={type} />
            </Container>
            <br />
        </div>
    )
}
export default Anime;