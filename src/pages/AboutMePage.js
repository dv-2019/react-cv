import React from 'react';
import Image from 'react-bootstrap/Image'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
// import profile from './profile.jpg'
import './aboutme.css'
class AboutMe extends React.Component {
    render() {
        return (
            <Container >
                <br />
                <Row className="rowAbout">
                    <Col sm={5}>
                        <div className="text-center">
                            <Image src="./profile.jpg" rounded className="profileImg" />
                            <br />
                            <h4>Poom</h4>
                        </div>
                    </Col>
                    <Col sm={7}>
                        <Row>
                            <Col>
                                <div>
                                    <h5><i class="fas fa-user"></i>&nbsp;<b>About Me</b></h5>
                                    <hr></hr>
                                    <p>Name: Poomrapee Kanthapong</p>
                                    <p>Age: 20</p>
                                    <p>Birthday: 26 January 1999</p>
                                </div>
                                
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <h5><i class="fas fa-address-book"></i>&nbsp;<b>Contact</b></h5>
                                <hr></hr>
                                <p>
                                    <a href="https://www.facebook.com/poomrapee.kanthapong" rel="noopener">
                                        <i class="fab fa-facebook-square fa-2x"></i>
                                    </a>
                                    &nbsp;&nbsp;
                                    <a href="https://www.instagram.com/poom_merz/" rel="noopener">
                                        <i class="fab fa-instagram fa-2x"></i>
                                    </a>
                                </p>
                                <div id="preferences"></div>
                                <p><i class="fas fa-phone-square fa-2x"></i>&nbsp;<b>: 091-0189030</b></p>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        );
    }
}
export default AboutMe;