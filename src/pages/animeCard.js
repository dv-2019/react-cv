import React from 'react';
import {  Col } from 'antd';
import './anime.css'
import { Card } from 'antd';
const { Meta } = Card;
const AnimeCard = (props) => {
    return (
        props.data.map((data, index) => {
            return (
                <Col span={6} key={index} className="gutter-row">
                    <div className="gutter-box">
                        <Card
                            pagination={{
                                onChange: page => {
                                    console.log(page);
                                },
                                pageSize: 5,
                            }}
                            cover={
                                <img
                                    alt={data.title}
                                    src={data.image_url}
                                />
                            }
                            // actions={[
                            //     <Button type="primary" size="default">
                            //         Primary
                            //     </Button>
                            // ]}
                        >
                            <Meta
                                style={{ height: 100 }}
                                // avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                                title={data.title}
                                description={data.synopsis}
                            />
                        </Card>,
                    </div>
                </Col>

            )
        })
    )
}
export default AnimeCard;