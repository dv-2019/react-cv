import React from 'react';
import { List } from 'antd';
import './anime.css'

import AnimeType from '../component/animeType';
const AnimeList = (props) => {
    
        return (
            <List
                itemLayout="vertical"
                size="large"
                loading={props.load}
                pagination={{
                    onChange: page => {
                        console.log(page);
                    },
                    pageSize: 5,
                }}
                dataSource={props.data}
                // footer={
                //     <div>
                //         <b>ant design</b> footer part
                //     </div>
                // }
                renderItem={item => (
                    <List.Item
                        key={item.title}
                        actions={[
                            <AnimeType type={item.type} />
                        ]}
                        extra={
                            <img
                                className="image"
                                alt="logo"
                                src={item.image_url}
                            />
                        }
                    >
                        <List.Item.Meta
                            // avatar={<Avatar src={item.avatar} />}
                            title={<a href={item.url}>{item.title}</a>}
                            description={
                                <>
                                    <label>{item.episodes}episodes</label>
                                </>
                            }
                        />
                        {item.synopsis}
                    </List.Item>
                )}
            />
        )
    

}


export default AnimeList;