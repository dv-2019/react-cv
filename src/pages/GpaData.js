
export const gpadatas = [
    {
        title: 'Semester 1 Year 2560',
        enrolled: [
            { NO: "1", CourseNo: "001101", CourseTitle: "FUNDAMENTAL ENGLISH 1", Credit: "3.00", Grade: "C+" },
            { NO: "2", CourseNo: "206113", CourseTitle: "CAL FOR SOFTWARE ENGINEERING", Credit: "3.00", Grade: "D" },
            { NO: "3", CourseNo: "751100", CourseTitle: "ECONOMICS FOR EVERYDAY LIFE", Credit: "3.00", Grade: "B+" },
            { NO: "4", CourseNo: "951100", CourseTitle: "MODERN LIFE AND ANIMATION", Credit: "3.00", Grade: "C" },
            { NO: "5", CourseNo: "953103", CourseTitle: "PROGRAMMING LOGICAL THINKING", Credit: "2.00	", Grade: "B" },
            { NO: "6", CourseNo: "953211", CourseTitle: "COMPUTER ORGANIZATION", Credit: "3.00", Grade: "B" }
        ],
        gpa: [
            { Record: "Semester 1/2560", CA: "17.00", CE: "17.00", GPA: "2.47", GPAALL: "2.47" }
        ]

    }
    , { 
        title: 'Semester 2 Year 2560',
        enrolled: [
            { NO: "1", CourseNo: "001102", CourseTitle: "FUNDAMENTAL ENGLISH 2	", Credit: "3.00", Grade: "C" },
            { NO: "2", CourseNo: "011251", CourseTitle: "LOGIC", Credit: "3.00", Grade: "C" },
            { NO: "3", CourseNo: "953102", CourseTitle: "ADT & PROBLEM SOLVING", Credit: "3.00", Grade: "B" },
            { NO: "4", CourseNo: "953104", CourseTitle: "WEB UI DESIGN & DEVELOP", Credit: "2.00", Grade: "B+" },
            { NO: "5", CourseNo: "953202", CourseTitle: "INTRODUCTION TO SE", Credit: "3.00", Grade: "C+" },
            { NO: "6", CourseNo: "953231", CourseTitle: "OBJECT ORIENTED PROGRAMMING", Credit: "3.00", Grade: "B" },
            { NO: "7", CourseNo: "955100", CourseTitle: "LEARNING THROUGH ACTIVITIES 1", Credit: "1.00", Grade: "A" }
        ],
        gpa: [
            { Record: "Semester 2/2560", CA: "18.00", CE: "18.00", GPA: "2.69", GPAALL: "2.59" }
        ]

    }
    , {
        title: 'Semester 1 Year 2561',
        enrolled: [
            { NO: "1", CourseNo: "001201", CourseTitle: "CRIT READ AND EFFEC WRITE", Credit: "3.00", Grade: "D" },
            { NO: "2", CourseNo: "206281", CourseTitle: "DISCRETE MATHEMATICS", Credit: "3.00", Grade: "C" },
            { NO: "3", CourseNo: "953212", CourseTitle: "DB SYS & DB SYS DESIGN", Credit: "3.00", Grade: "B" },
            { NO: "4", CourseNo: "953233", CourseTitle: "PROGRAMMING METHODOLOGY", Credit: "3.00", Grade: "C+" },
            { NO: "5", CourseNo: "953261", CourseTitle: "INTERACTIVE WEB DEVELOPMENT", Credit: "2.00", Grade: "A" },
            { NO: "6", CourseNo: "953361", CourseTitle: "COMP NETWORK & PROTOCOLS", Credit: "3.00", Grade: "F" }
        ],
        gpa: [
            { Record: "Semester 1/2561", CA: "17.00", CE: "14.00", GPA: "1.97", GPAALL: "2.38" }
        ]
    }
    , {
        title: 'Semester 2 Year 2561',
        enrolled: [
            { NO: "1", CourseNo: "001225", CourseTitle: "ENGL IN SCIENCE & TECH CONT", Credit: "3.00", Grade: "C+" },
            { NO: "2", CourseNo: "057122", CourseTitle: "SWIMMING FOR LIFE AND EXERCISE", Credit: "1.00", Grade: "A" },
            { NO: "3", CourseNo: "206255", CourseTitle: "MATH FOR SOFTWARE TECH", Credit: "3.00", Grade: "B" },
            { NO: "4", CourseNo: "953201", CourseTitle: "ALGO DESIGN & ANALYSIS", Credit: "3.00", Grade: "C+" },
            { NO: "5", CourseNo: "953214", CourseTitle: "OS & PROG LANG PRINCIPLES", Credit: "3.00", Grade: "B" },
            { NO: "6", CourseNo: "953232", CourseTitle: "OO ANALYSIS & DESIGN", Credit: "3.00", Grade: "C+" },
            { NO: "7", CourseNo: "953234", CourseTitle: "ADVANCED SOFTWARE DEVELOPMENT", Credit: "3.00", Grade: "B+" },
            { NO: "8", CourseNo: "955200", CourseTitle: "LEARNING THROUGH ACTIVITIES 2", Credit: "1.00", Grade: "B" }
        ],
        gpa: [
            { Record: "Semester 2/2561", CA: "20.00", CE: "20.00", GPA: "2.90", GPAALL: "2.53" }
        ]
    }, {
        title: 'Semester 1 Year 2562',
        enrolled: [
            { NO: "1", CourseNo: "208263", CourseTitle: "ELEMENTARY STATISTICS", Credit: "3.00", Grade: "C" },
            { NO: "2", CourseNo: "259109", CourseTitle: "TELECOM IN THAILAND", Credit: "3.00", Grade: "C+" },
            { NO: "3", CourseNo: "269111", CourseTitle: "COMMU TECH IN A CHANGING WORLD", Credit: "3.00", Grade: "C" },
            { NO: "4", CourseNo: "888102", CourseTitle: "BIG DATA FOR BUSINESS", Credit: "3.00", Grade: "A" },
            { NO: "5", CourseNo: "953321", CourseTitle: "SOFTWARE REQ ANALYSIS", Credit: "3.00", Grade: "C+" },
            { NO: "6", CourseNo: "953322", CourseTitle: "SOFTWARE DESIGN & ARCH", Credit: "3.00", Grade: "C" },
            { NO: "7", CourseNo: "953331", CourseTitle: "COMPO-BASED SOFTWARE DEV", Credit: "3.00", Grade: "B+" }
        ],
        gpa: [
            { Record: "Semester 1/2562", CA: "21.00", CE: "21.00", GPA: "2.64", GPAALL: "2.55" }
        ]
    }
]
export default {
    gpadatas
}