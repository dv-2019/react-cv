import React from 'react';
import TableGpa from '../component/TableGpa';
import Nodata from '../component/Nodata';
const GpaFunc = (props) => {
    if (props.semValue === 'All' && props.yearValue === 'All') {
        return (
            props.sems.map((data, index) => {
                return (
                    <div key={index}>
                        <TableGpa
                            data={data}
                        />
                    </div>
                )

            })
        );
    } else if (props.semValue === '1' && props.yearValue === '2560') {
        return (
            <TableGpa
                data={props.sems[0]}
            />
        )
    }
    else if (props.semValue === '2' && props.yearValue === '2560') {
        return (
            <TableGpa
                data={props.sems[1]}
            />
        )
    }
    else if (props.semValue === '1' && props.yearValue === '2561') {
        return (
            <TableGpa
                data={props.sems[2]}
            />
        )
    }
    else if (props.semValue === '2' && props.yearValue === '2561') {
        return (
            <TableGpa
                data={props.sems[3]}
            />
        )
    }
    else if (props.semValue === '1' && props.yearValue === '2562') {
        return (
            <TableGpa
                data={props.sems[4]}
            />
        )
    }
    else {
        return (
            <Nodata />
        )
    }
}
export default GpaFunc;