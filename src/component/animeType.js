import React from 'react';
import { Tag } from 'antd';
const AnimeType = (props) => {
    if (props.type === 'TV') {
        return (
            <Tag color="geekblue">TV</Tag>
        )
    } else if (props.type === 'Movie') {
        return (
            <Tag color="green">Movie</Tag>
        )
    } else if (props.type === 'Special') {
        return (
            <Tag color="gold">Special</Tag>
        )
    } else if (props.type === 'OVA') {
        return (
            <Tag color="red">OVA</Tag>
        )
    }

}
export default AnimeType;