import React from 'react';
import { Navbar, Nav } from 'react-bootstrap'

class Header extends React.Component {
    render() {
        return (
            <Navbar bg="dark" variant="dark" sticky="top">
                <Navbar.Brand href="/home">CV</Navbar.Brand>
                <Nav className="mr-auto">
                    <Nav.Link href="/home#aboutme">About Me</Nav.Link>
                    <Nav.Link href="/home#preferences">Preferences</Nav.Link>
                    <Nav.Link href="/home#skill">Speciality</Nav.Link>
                    <Nav.Link href="/home#history">History</Nav.Link>
                    <Nav.Link href="/anime">Anime</Nav.Link>
                    <Nav.Link href="/gpa">GPA</Nav.Link>
                </Nav>
            </Navbar>
        )
    }
}
export default Header;