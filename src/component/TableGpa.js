import React from 'react';
import Table from 'react-bootstrap/Table'
const TableGpa = (props) => {

    return (
        <div>
            <div className="row justify-content-center">
                <div>
                    <hr />
                    <h3><i class="fas fa-calendar"></i> &nbsp;{props.data.title}</h3>
                    <hr />
                </div>

            </div>
            <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>Course No</th>
                        <th>Course Title</th>
                        <th>Credit</th>
                        <th>Grade</th>
                    </tr>
                </thead>
                <tbody>
                    {props.data.enrolled.map((data, index) => {
                        return (
                            <tr key={index}>
                                <td>{data.NO}</td>
                                <td>{data.CourseNo}</td>
                                <td>{data.CourseTitle}</td>
                                <td>{data.Credit}</td>
                                <td>{data.Grade}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
            <br />
            <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                        <th>Record</th>
                        <th>CA</th>
                        <th>CE</th>
                        <th>GPA</th>
                        <th>GPA ALL</th>
                    </tr>
                </thead>
                <tbody>
                    {props.data.gpa.map((data, index) => {
                        return (
                            <tr key={index}>
                                <td>{data.Record}</td>
                                <td>{data.CA}</td>
                                <td>{data.CE}</td>
                                <td>{data.GPA}</td>
                                <td>{data.GPAALL}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
        </div>
    );

}
export default TableGpa;