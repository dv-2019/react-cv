import React from 'react';
import Header from './component/Header.js';
// import AboutMe from './pages/AboutMePage.js';
import Skill from './pages/SkillBar.js'
import Gpa from './pages/gpa.js'
import Login from './pages/loginpage';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import Home from './pages/home.js';
import Anime from './pages/anime.js'
// setter
localStorage.setItem("isLogin", false)
// getter
const MainRouting = () => {
    return (
        <BrowserRouter>
            <Route path="/processLogin" render={() => {
                // alert('Login here')
                localStorage.setItem("isLogin", true)
                return <Redirect to="/gpa" />
            }} />
            <Route path="/processLogout" render={() => {
                // alert('Login here')
                localStorage.setItem("isLogin", false)
                return <Redirect to="/aboutme" />

            }} />

            <Route path="/" render={() => {
                return (
                    <Redirect to="/home" />
                )
            }}exact={true}></Route>
            <Route path="/" component={Header}></Route>
            <Route path="/home" component={Home}></Route>
            <Route path="/skill" component={Skill}></Route>
            <Route path="/gpa" component={Gpa}></Route>
            <Route path="/anime" component={Anime}></Route>
            <Route path="/login" component={Login}></Route>
        </BrowserRouter>
    )

}
export default MainRouting;